package com.zaboo.ZombieBird.android;

//import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
//import com.zaboo.ZombieBird.ZBGame;

public class AndroidLauncher extends com.badlogic.gdx.backends.android.AndroidApplication {
	
	private static AndroidLauncher instance;

	@Override
	protected void onCreate(final android.os.Bundle savedInstanceState) {
		instance = this;
		super.onCreate(savedInstanceState);
		
		final com.badlogic.gdx.backends.android.AndroidApplicationConfiguration config = new com.badlogic.gdx.backends.android.AndroidApplicationConfiguration();
		initialize(new com.zaboo.ZombieBird.ZBGame(), config);
	}
}
