sprite.png
format: RGBA8888
filter: Linear,Linear
repeat: none

play-on
  rotate: false
  xy: 0, 18
  size: 29, 16
  orig: 29, 16
  offset: 0, 0
  index: -1
play-off
  rotate: false
  xy: 29, 18
  size: 29, 16
  orig: 29, 16
  offset: 0, 0
  index: -1

text-ready
  rotate: false
  xy: 0, 9
  size: 34, 7
  orig: 34, 7
  offset: 0, 0
  index: -1
text-retry
  rotate: false
  xy: 36, 9
  size: 33, 7
  orig: 33, 7
  offset: 0, 0
  index: -1
text-gameover
  rotate: false
  xy: 0, 0
  size: 46, 7
  orig: 46, 7
  offset: 0, 0
  index: -1
text-highscore
  rotate: false
  xy: 48, 0
  size: 48, 7
  orig: 48, 7
  offset: 0, 0
  index: -1

board
  rotate: false
  xy: 0, 36
  size: 97, 37
  orig: 97, 37
  offset: 0, 0
  index: -1
star1
  rotate: false
  xy: 71, 9
  size: 10, 10
  orig: 10, 10
  offset: 0, 0
  index: -1
star2
  rotate: false
  xy: 83, 9
  size: 10, 10
  orig: 10, 10
  offset: 0, 0
  index: -1
