package com.zaboo.ZombieBird;

import com.badlogic.gdx.Game;
import com.zaboo.Helpers.AssetLoader;
import com.zaboo.Screens.SplashScreen;

public class ZBGame extends Game {

	@Override
	public void create() {
		AssetLoader.load();
		setScreen(new SplashScreen(this));
	}

	@Override
	public void dispose() {
		super.dispose();
		AssetLoader.dispose();
	}

}
