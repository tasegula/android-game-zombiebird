package com.zaboo.GameWorld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.zaboo.GameObjects.Bird;
import com.zaboo.GameObjects.Cloud;
import com.zaboo.GameObjects.Grass;
import com.zaboo.GameObjects.Pipe;
import com.zaboo.GameObjects.ScrollHandler;
import com.zaboo.Helpers.AssetLoader;
import com.zaboo.Helpers.InputHandler;
import com.zaboo.TweenAccessors.Value;
import com.zaboo.TweenAccessors.ValueAccessor;
import com.zaboo.config.Prop;
import com.zaboo.config.ResourceManager;
import com.zaboo.ui.SimpleButton;

import java.util.List;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

public class GameRenderer {

	private final GameWorld myWorld;
	private final OrthographicCamera cam;

	private final ShapeRenderer shapeRenderer;
	private final SpriteBatch batcher;

	private final int midPointY;

	// Game Objects
	private Bird bird;
	private ScrollHandler scroller;
	private Grass[] grass = new Grass[2];
	private Pipe[] pipe = new Pipe[3];
	private Cloud[] cloud = new Cloud[3];

	// Game Assets
	private Animation birdAnimation;
	private TextureRegion bg, grassTex;
	private TextureRegion birdMid;
	private TextureRegion skullUp, skullDown, bar;
	private TextureRegion ready, gameOver, highScore, retry;
	private TextureRegion scoreboard, starOn, starOff;
	private TextureRegion[] cloudTex = new TextureRegion[3];

	// Tween stuff
	private TweenManager manager;
	private Value alpha = new Value();

	// Buttons
	private final List<SimpleButton> menuButtons;
	private Color transitionColor;

	public GameRenderer(final GameWorld world, final int gameHeight, final int midPointY) {
		this.myWorld = world;

		this.midPointY = midPointY;
		this.menuButtons = ((InputHandler) Gdx.input.getInputProcessor()).getMenuButtons();

		this.cam = new OrthographicCamera();
		cam.setToOrtho(true, 136, gameHeight);

		// Attach batcher and shapeRender to camera
		batcher = new SpriteBatch();
		batcher.setProjectionMatrix(cam.combined);
		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setProjectionMatrix(cam.combined);

		// Call helper methods to initalize instance variables
		initGameObjects();
		initAssets();
		
		transitionColor = new Color();
		prepareTransition(255, 255, 255, 0.5f);
	}

	// ************************************************************************
	// INITIALIZE AND SETUP
	// ************************************************************************

	private void initGameObjects() {
		bird = myWorld.getBird();
		scroller = myWorld.getScroller();

		grass[0] = scroller.getGrass(0);
		grass[1] = scroller.getGrass(1);
		pipe[0] = scroller.getPipe(0);
		pipe[1] = scroller.getPipe(1);
		pipe[2] = scroller.getPipe(2);

		for (int i = 0; i < 3; i++) {
			cloud[i] = scroller.getCloud(i);
		}
	}

	private void initAssets() {
		bg = AssetLoader.bg;
		grassTex = AssetLoader.grass;

		birdAnimation = AssetLoader.birdAnimation;
		birdMid = AssetLoader.bird[2];

		skullUp = AssetLoader.skullUp;
		skullDown = AssetLoader.skullDown;
		bar = AssetLoader.bar;

		ready = AssetLoader.ready;
		retry = AssetLoader.retry;
		gameOver = AssetLoader.gameOver;
		highScore = AssetLoader.highScore;

		scoreboard = AssetLoader.scoreboard;
		starOn = AssetLoader.starOn;
		starOff = AssetLoader.starOff;
		
		cloudTex[0] = AssetLoader.cloud[0];
		cloudTex[1] = AssetLoader.cloud[1];
		cloudTex[2] = AssetLoader.cloud[2];
	}

	// ************************************************************************
	// DRAW
	// ************************************************************************

	private void drawGrass() {
		// Draw the grass
		batcher.draw(grassTex, grass[0].getX(), grass[0].getY(),
					 grass[0].getWidth(), grass[0].getHeight());
		batcher.draw(grassTex, grass[1].getX(), grass[1].getY(),
					 grass[1].getWidth(), grass[1].getHeight());
	}
	
	private void drawCloud() {
		// Draw the grass
		for (int i = 0; i < 3; i++) {
			batcher.draw(cloudTex[i], cloud[i].getX(), cloud[i].getY(),
			             cloud[i].getWidth(), cloud[i].getHeight());
		}
	}

	private void drawSkulls() {
		// Temporary code! Sorry about the mess :)
		// We will fix this when we finish the Pipe class.

		batcher.draw(skullUp,
					 pipe[0].getX() - 1, pipe[0].getY() + pipe[0].getHeight() - 14,
					 24, 14);
		batcher.draw(skullDown,
					 pipe[0].getX() - 1, pipe[0].getY() + pipe[0].getHeight() + 45,
					 24, 14);

		batcher.draw(skullUp,
					 pipe[1].getX() - 1, pipe[1].getY() + pipe[1].getHeight() - 14,
					 24, 14);
		batcher.draw(skullDown,
					 pipe[1].getX() - 1, pipe[1].getY() + pipe[1].getHeight() + 45,
					 24, 14);

		batcher.draw(skullUp,
					 pipe[2].getX() - 1, pipe[2].getY() + pipe[2].getHeight() - 14,
					 24, 14);
		batcher.draw(skullDown,
					 pipe[2].getX() - 1, pipe[2].getY() + pipe[2].getHeight() + 45,
					 24, 14);
	}

	private void drawPipes() {
		// Temporary code! Sorry about the mess :)
		// We will fix this when we finish the Pipe class.
		batcher.draw(bar, pipe[0].getX(), pipe[0].getY(),
					 pipe[0].getWidth(), pipe[0].getHeight());
		batcher.draw(bar, pipe[0].getX(), pipe[0].getY() + pipe[0].getHeight() + 45,
					 pipe[0].getWidth(), midPointY + 66 - (pipe[0].getHeight() + 45));

		batcher.draw(bar, pipe[1].getX(), pipe[1].getY(),
					 pipe[1].getWidth(), pipe[1].getHeight());
		batcher.draw(bar, pipe[1].getX(), pipe[1].getY() + pipe[1].getHeight() + 45,
					 pipe[1].getWidth(), midPointY + 66 - (pipe[1].getHeight() + 45));

		batcher.draw(bar, pipe[2].getX(), pipe[2].getY(),
					 pipe[2].getWidth(), pipe[2].getHeight());
		batcher.draw(bar, pipe[2].getX(), pipe[2].getY() + pipe[2].getHeight() + 45,
					 pipe[2].getWidth(), midPointY + 66 - (pipe[2].getHeight() + 45));
	}

	private void drawBirdCentered(final float runTime) {
		batcher.draw(birdAnimation.getKeyFrame(runTime),
					 59, bird.getY() - 15,
					 bird.getWidth() / 2.0f, bird.getHeight() / 2.0f,
					 bird.getWidth(), bird.getHeight(),
					 1, 1, bird.getRotation());
	}

	private void drawBird(final float runTime) {
		if (bird.shouldFlap()) {
			batcher.draw(birdAnimation.getKeyFrame(runTime),
						 bird.getX(), bird.getY(),
						 bird.getWidth() / 2.0f, bird.getHeight() / 2.0f,
						 bird.getWidth(), bird.getHeight(),
						 1, 1, bird.getRotation());
		}
		else {
			batcher.draw(birdMid,
						 bird.getX(), bird.getY(),
						 bird.getWidth() / 2.0f, bird.getHeight() / 2.0f,
						 bird.getWidth(), bird.getHeight(),
						 1, 1, bird.getRotation());
		}
	}

	private void drawMenuUI() {
		drawText("FlyingBird", 136 / 2 - 40, midPointY - 50);

		for (final SimpleButton button : menuButtons) {
			button.draw(batcher);
		}
	}

	private void drawScoreboard() {
		batcher.draw(scoreboard, 22, midPointY - 30, 97, 37);
		
		batcher.draw(starOff, 25, midPointY - 15, 10, 10);
		batcher.draw(starOff, 37, midPointY - 15, 10, 10);
		batcher.draw(starOff, 49, midPointY - 15, 10, 10);
		batcher.draw(starOff, 61, midPointY - 15, 10, 10);
		batcher.draw(starOff, 73, midPointY - 15, 10, 10);
		
		if (myWorld.getScore() > 2) {
			batcher.draw(starOn, 73, midPointY - 15, 10, 10);
		}
		if (myWorld.getScore() > 17) {
			batcher.draw(starOn, 61, midPointY - 15, 10, 10);
		}
		if (myWorld.getScore() > 50) {
			batcher.draw(starOn, 49, midPointY - 15, 10, 10);
		}
		if (myWorld.getScore() > 80) {
			batcher.draw(starOn, 37, midPointY - 15, 10, 10);
		}
		if (myWorld.getScore() > 120) {
			batcher.draw(starOn, 25, midPointY - 15, 10, 10);
		}
		
		String text = "" + myWorld.getScore();
		int length = text.length();
		
		ResourceManager.getFont(Prop.FONT_WHITE).draw(batcher, text, 104 - (2 * length), midPointY - 20);
		
		text = "" + AssetLoader.getHighScore();
		length = text.length();
		ResourceManager.getFont(Prop.FONT_WHITE).draw(batcher, text, 104 - (2.5f * length), midPointY - 3);
		
	}

	private void drawRetry() {
		batcher.draw(retry, 36, midPointY + 10, 66, 14);
	}
	
	private void drawReady() {
		batcher.draw(ready, 36, midPointY - 50, 68, 14);
	}
	
	private void drawGameOver() {
		batcher.draw(gameOver, 24, midPointY - 50, 92, 14);
	}

	private void drawHighScore() {
		batcher.draw(highScore, 22, midPointY - 50, 96, 14);
	}

	private void drawScore() {
		final String score = "" + myWorld.getScore();
		drawText(score, 68 - 3 * score.length(), midPointY - 82);
	}

	@Deprecated
	private void drawText(final String message, final int x, final int y) {
		// Draw shadow first, then text
		ResourceManager.getFont(Prop.FONT_SHADOW).draw(batcher, message, x, y);
		ResourceManager.getFont(Prop.FONT_GREEN).draw(batcher, message, x - 1, y - 1);
	}

	// ************************************************************************
	// RENDER
	// ************************************************************************

	public void render(final float delta, final float runTime) {
		// Fill the entire screen with black, to prevent potential flickering.
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// Begin ShapeRenderer
		shapeRenderer.begin(ShapeType.Filled);

		// Draw Background color
		float r = 218 / 255.0f;
		float g = 227 / 255.0f;
		float b = 243 / 255.0f;
		shapeRenderer.setColor(r, g, b, 1);
		shapeRenderer.rect(0, 0, 136, midPointY + 66);

		// Draw Grass
		shapeRenderer.setColor(111 / 255.0f, 186 / 255.0f, 45 / 255.0f, 1);
		shapeRenderer.rect(0, midPointY + 66, 136, 11);

		// Draw Dirt
		shapeRenderer.setColor(147 / 255.0f, 80 / 255.0f, 27 / 255.0f, 1);
		shapeRenderer.rect(0, midPointY + 77, 136, 52);

		// End ShapeRenderer
		shapeRenderer.end();

		// Begin SpriteBatch
		batcher.begin();
		// Disable transparency; good for performance when drawing images that
		// do not require transparency.
		// batcher.disableBlending();
		batcher.draw(bg, 0, midPointY + 23, 136, 43);

		// Draw Grass and Pipes
		drawCloud();
		drawPipes();

		// We need transparency; enable it;
		// batcher.enableBlending();
		drawSkulls();

		if (myWorld.isRunning()) {
			drawBird(runTime);
			drawScore();
		}
		else if (myWorld.isReady()) {
			drawBird(runTime);
			drawReady();
		}
		else if (myWorld.isMenu()) {
			drawBirdCentered(runTime);
			drawMenuUI();
		}
		else if (myWorld.isGameOver()) {
			drawScoreboard();
			drawBird(runTime);
			drawGameOver();
			drawRetry();
		}
		else if (myWorld.isHighScore()) {
			drawScoreboard();
			drawBird(runTime);
			drawHighScore();
			drawRetry();
		}
		
		drawGrass();

		// End SpriteBatch
		batcher.end();
		drawTransition(delta);
	}
	
	public void prepareTransition(int r, int g, int b, float duration) {
		transitionColor.set(r / 255.0f, g / 255.0f, b / 255.0f, 1);
		alpha.setValue(1);
		
		Tween.registerAccessor(Value.class, new ValueAccessor());
		manager = new TweenManager();
		
		Tween.to(alpha, -1, duration)
			 .target(0)
			 .ease(TweenEquations.easeOutQuad)
			 .start(manager);
	}

	private void drawTransition(final float delta) {
		if (alpha.getValue() > 0) {
			manager.update(delta);

			Gdx.gl.glEnable(GL20.GL_BLEND);
			Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

			shapeRenderer.begin(ShapeType.Filled);
			
			shapeRenderer.setColor(transitionColor.r,
								   transitionColor.g,
								   transitionColor.b,
								   alpha.getValue());
			shapeRenderer.rect(0, 0, 136, 300);
			shapeRenderer.end();

			Gdx.gl.glDisable(GL30.GL_BLEND);
		}
	}

}
