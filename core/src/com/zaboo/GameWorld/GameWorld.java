package com.zaboo.GameWorld;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.zaboo.GameObjects.Bird;
import com.zaboo.GameObjects.ScrollHandler;
import com.zaboo.Helpers.AssetLoader;
import com.zaboo.config.Prop;
import com.zaboo.config.ResourceManager;

public class GameWorld {

	private GameRenderer renderer;

	private final Bird bird;
	private final ScrollHandler scroller;
	private final Rectangle ground;

	private int score = 0;
	private float runTime = 0;
	private final int midPointY;

	private GameState currentState;

	public enum GameState {
		MENU, READY, RUNNING, GAMEOVER, HIGHSCORE
	}

	public GameWorld(final int midPointY) {
		currentState = GameState.MENU;
		this.midPointY = midPointY;

		bird = new Bird(33, midPointY - 5, 17, 12);
		// The grass should start 66 pixels below the midPointY
		scroller = new ScrollHandler(this, midPointY + 66);
		ground = new Rectangle(0, midPointY + 66, 137, 11);
	}

	public void update(final float delta) {
		runTime += delta;

		switch (currentState) {
		case READY:
		case MENU:
			updateReady(delta);
			break;

		case RUNNING:
			updateRunning(delta);
			break;
		default:
			break;
		}
	}

	private void updateReady(final float delta) {
		bird.updateReady(runTime);
		scroller.updateReady(delta);
	}

	private void updateRunning(float delta) {
		// if game takes too long to update, won't break the collision detection
		if (delta > 0.15f) {
			delta = 0.15f;
		}

		bird.update(delta);
		scroller.update(delta);

		if (scroller.collides(bird) && bird.isAlive()) {
			// CleanUp on GameOver
			scroller.stop();
			bird.die();
			ResourceManager.getSound(Prop.AUDIO_DEAD).play();
			
			renderer.prepareTransition(255, 255, 255, 0.3f);
			ResourceManager.getSound(Prop.AUDIO_FALL).play();
		}

		if (Intersector.overlaps(bird.getBoundingCircle(), ground)) {
			
			if (bird.isAlive()) {
				bird.die();
				ResourceManager.getSound(Prop.AUDIO_DEAD).play();
				renderer.prepareTransition(255, 255, 255, .3f);
			}

			scroller.stop();
			bird.decelerate();

			currentState = GameState.GAMEOVER;

			if (score > AssetLoader.getHighScore()) {
				AssetLoader.setHighScore(score);
				currentState = GameState.HIGHSCORE;
			}
		}
	}

	public Bird getBird() {
		return bird;
	}

	public int getMidPointY() {
		return midPointY;
	}

	public ScrollHandler getScroller() {
		return scroller;
	}

	public int getScore() {
		return score;
	}

	public void addScore(final int increment) {
		score += increment;
	}

	public void start() {
		currentState = GameState.RUNNING;
	}

	public void ready() {
		currentState = GameState.READY;
		renderer.prepareTransition(0, 0, 0, 1.0f);
	}

	public void restart() {
		score = 0;
		bird.onRestart(midPointY - 5);
		scroller.onRestart();
		ready();
	}

	public boolean isReady() {
		return currentState == GameState.READY;
	}

	public boolean isGameOver() {
		return currentState == GameState.GAMEOVER;
	}

	public boolean isHighScore() {
		return currentState == GameState.HIGHSCORE;
	}

	public boolean isMenu() {
		return currentState == GameState.MENU;
	}

	public boolean isRunning() {
		return currentState == GameState.RUNNING;
	}
	
	public void setRenderer(GameRenderer renderer) {
		this.renderer = renderer;
	}
}
