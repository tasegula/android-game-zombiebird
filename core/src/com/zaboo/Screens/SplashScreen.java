package com.zaboo.Screens;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.zaboo.Helpers.AssetLoader;
import com.zaboo.TweenAccessors.SpriteAccessor;
import com.zaboo.ZombieBird.ZBGame;

public class SplashScreen implements Screen {

	private TweenManager manager;
	private SpriteBatch batcher;
	private Sprite sprite;
	private ZBGame game;

	public SplashScreen(final ZBGame game) {
		this.game = game;
	}

	@Override
	public void show() {
		sprite = new Sprite(AssetLoader.logo);
		sprite.setColor(1, 1, 1, 0);

		final float width = Gdx.graphics.getWidth();
		final float height = Gdx.graphics.getHeight();
		final float desiredWidth = width * 0.7f;
		final float scale = desiredWidth / sprite.getWidth();

		sprite.setSize(sprite.getWidth() * scale, sprite.getHeight() * scale);
		sprite.setPosition(width / 2 - sprite.getWidth() / 2,
						   height / 2 - sprite.getHeight() / 2);
		setupTween();
		batcher = new SpriteBatch();
	}

	private void setupTween() {
		Tween.registerAccessor(Sprite.class, new SpriteAccessor());
		manager = new TweenManager();

		final TweenCallback cb = new TweenCallback() {

			@Override
			public void onEvent(final int type, final BaseTween<?> source) {
				game.setScreen(new GameScreen());
			}
		};

		Tween.to(sprite, SpriteAccessor.ALPHA, 0.8f)
			 .target(1)
			 .ease(TweenEquations.easeInOutQuad)
			 .repeatYoyo(1, 0.4f)
			 .setCallback(cb)
			 .setCallbackTriggers(TweenCallback.COMPLETE)
			 .start(manager);

	}

	@Override
	public void render(final float delta) {
		manager.update(delta);
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batcher.begin();
		sprite.draw(batcher);
		batcher.end();
	}

	@Override
	public void resize(final int width, final int height) {
		// TODO
	}

	@Override
	public void hide() {
		// TODO
	}

	@Override
	public void pause() {
		// TODO
	}

	@Override
	public void resume() {
		// TODO
	}

	@Override
	public void dispose() {
		// TODO
	}

}
