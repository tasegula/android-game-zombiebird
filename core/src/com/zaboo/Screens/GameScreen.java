package com.zaboo.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.zaboo.GameWorld.GameRenderer;
import com.zaboo.GameWorld.GameWorld;
import com.zaboo.Helpers.InputHandler;

public class GameScreen implements Screen {

	private static final String CLASS = "GameScreen";

	private final GameWorld world;
	private final GameRenderer renderer;

	private float runTime;

	public GameScreen() {
		final float screenWidth = Gdx.graphics.getWidth();
		final float screenHeight = Gdx.graphics.getHeight();
		final float gameWidth = 136;
		final float gameHeight = screenHeight / (screenWidth / gameWidth);

		final int midPointY = (int) (gameHeight / 2);

		world = new GameWorld(midPointY);

		final float scaleFactorX = screenWidth / gameWidth;
		final float scaleFactorY = screenHeight / gameHeight;

		Gdx.input.setInputProcessor(new InputHandler(world, scaleFactorX, scaleFactorY));
		renderer = new GameRenderer(world, (int) gameHeight, midPointY);
		world.setRenderer(renderer);
	}

	@Override
	public void render(final float delta) {
		runTime += delta;
		world.update(delta);
		renderer.render(delta, runTime);
	}

	@Override
	public void resize(final int width, final int height) {
		Gdx.app.log(CLASS, "resizing");
	}

	@Override
	public void show() {
        // TODO
		// Gdx.app.log(CLASS, "show called");
	}

	@Override
	public void hide() {
        // TODO
		// Gdx.app.log(CLASS, "hide called");
	}

	@Override
	public void pause() {
        // TODO
		// Gdx.app.log(CLASS, "pause called");
	}

	@Override
	public void resume() {
        // TODO
		// Gdx.app.log(CLASS, "resume called");
	}

	@Override
	public void dispose() {
		// Leaved blank
	}

}
