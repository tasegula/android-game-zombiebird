package com.zaboo.TweenAccessors;

public class Value {

	private float val = 1;

	public float getValue() {
		return val;
	}

	public void setValue(final float newVal) {
		val = newVal;
	}

}
