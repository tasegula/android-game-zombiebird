package com.zaboo.Helpers;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.zaboo.GameObjects.Bird;
import com.zaboo.GameWorld.GameWorld;
import com.zaboo.config.Util;
import com.zaboo.ui.SimpleButton;

public class InputHandler implements InputProcessor {

	private final Bird myBird;
	private final GameWorld myWorld;

	private final List<SimpleButton> menuButtons;
	private final SimpleButton playButton;

	private final float scaleFactorX;
	private final float scaleFactorY;

	public InputHandler(final GameWorld gameWorld,
						final float scaleFactorX, final float scaleFactorY) {
		this.myWorld = gameWorld;
		this.myBird = myWorld.getBird();

		final int midPointY = myWorld.getMidPointY();

		this.scaleFactorX = scaleFactorX;
		this.scaleFactorY = scaleFactorY;

		menuButtons = new ArrayList<SimpleButton>();
		playButton = new SimpleButton(136 / 2 - AssetLoader.playButtonUp.getRegionWidth() / 2,
									  midPointY + 50,
									  29, 16, // dimensions
									  AssetLoader.playButtonUp,
									  AssetLoader.playButtonDown);
		menuButtons.add(playButton);
	}

	@Override
	public boolean touchDown(int screenX, int screenY, final int pointer, final int button) {
		screenX = scaleX(screenX);
		screenY = scaleY(screenY);

		if (myWorld.isMenu()) {
			playButton.isTouchDown(screenX, screenY);
		}
		else if (myWorld.isReady()) {
			myWorld.start();
			myBird.onClick();
		}
		else if (myWorld.isRunning()) {
			myBird.onClick();
		}
		
		if (myWorld.isGameOver() || myWorld.isHighScore()) {
			myWorld.restart();
		}

		return true; // Return true to say we handled the touch.
	}

	@Override
	public boolean touchUp(int screenX, int screenY, final int pointer, final int button) {
		screenX = scaleX(screenX);
		screenY = scaleY(screenY);

		if (myWorld.isMenu()) {
			if (playButton.isTouchUp(screenX, screenY)) {
				myWorld.ready();
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean keyDown(final int keycode) {
		// Can now use Space Bar to play the game
		if (keycode == Keys.SPACE) {

			if (myWorld.isMenu()) {
				myWorld.ready();
			}
			else if (myWorld.isReady()) {
				myWorld.start();
			}

			myBird.onClick();

			if (myWorld.isGameOver() || myWorld.isHighScore()) {
				myWorld.restart();
			}
		}

		return false;
	}

	@Override
	public boolean keyUp(final int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(final char character) {
		return false;
	}

	@Override
	public boolean touchDragged(final int screenX, final int screenY, final int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(final int screenX, final int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(final int amount) {
		return false;
	}

	private int scaleX(final int screenX) {
		return (int) (screenX / scaleFactorX);
	}

	private int scaleY(final int screenY) {
		return (int) (screenY / scaleFactorY);
	}

	public List<SimpleButton> getMenuButtons() {
		return menuButtons;
	}

}
