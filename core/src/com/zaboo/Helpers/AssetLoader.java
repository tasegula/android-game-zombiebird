package com.zaboo.Helpers;

import java.util.Collection;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.zaboo.config.Prop;
import com.zaboo.config.ResourceManager;
import com.zaboo.config.SceneManager;
import com.zaboo.config.GameData.GameTexture;
import com.zaboo.config.GameData.SimpleData;

public class AssetLoader {
	
	public static TextureRegion logo;
	public static TextureRegion bg, grass;
	public static TextureRegion[] bird;
	public static TextureRegion[] cloud;
	public static TextureRegion skullUp, skullDown, bar;
	
	public static TextureRegion playButtonUp, playButtonDown;
	public static TextureRegion ready, gameOver, highScore, retry;
	public static TextureRegion scoreboard, starOn, starOff;
	
	public static Animation birdAnimation;
	public static Preferences prefs;
	
	private static final String HIGHSCORE = "highScore";
	
	public static void load() {
		ResourceManager.load();
		SceneManager.load();

		loadLogo();
		loadBird();
		loadCloud();
		loadBackground();
		loadPipe();
		
		loadSprites();
		
		// Create (or retrieve existing) preferences file
		prefs = Gdx.app.getPreferences("ZombieBird");
		
		// Provide default high score of 0
		if (!prefs.contains(HIGHSCORE)) {
			prefs.putInteger(HIGHSCORE, 0);
		}
	}

	// Receives an integer and maps it to the String highScore in prefs
	public static void setHighScore(final int val) {
		prefs.putInteger(HIGHSCORE, val);
		prefs.flush();
	}

	// Retrieves the current high score
	public static int getHighScore() {
		return prefs.getInteger(HIGHSCORE);
	}

	public static void dispose() {
		// We must dispose of the textures when we are finished.
		Collection<Texture> textures = ResourceManager.getTexturesMap().values();
		for (Texture texture : textures) {
			texture.dispose();
		}

		Collection<Sound> sounds = ResourceManager.getSoundsMap().values();
		for (Sound sound : sounds) {
			sound.dispose();
		}

		Collection<BitmapFont> fonts = ResourceManager.getFontsMap().values();
		for (BitmapFont font : fonts) {
			font.dispose();
		}
	}

	// ************************************************************************
	// HELPERS
	// ************************************************************************

	private static void loadLogo() {
		// get Company Logo
		GameTexture tex = SceneManager.gameTexture(Prop.TEX_LOGO);
		Texture texture = ResourceManager.getTexture(tex.name);
		SimpleData data = tex.reg[0];
		logo = new TextureRegion(texture, data.x, data.y, data.w, data.h);
	}
	
	private static void loadBird() {
		GameTexture tex = SceneManager.gameTexture(Prop.TEX_BIRD);
		Texture texture = ResourceManager.getTexture(tex.name);

		// ignore bird[0]
		int size = tex.size - 1;
		bird = new TextureRegion[size];
		
		for (int i = 0; i < size; i++) {
			bird[i] = getTexFromData(texture, tex.reg[i + 1]);
		}

		// Create bird animation
		birdAnimation = new Animation(0.06f, bird);
		birdAnimation.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);

	}
	
	private static void loadCloud() {
		GameTexture tex = SceneManager.gameTexture(Prop.TEX_CLOUD);
		Texture texture = ResourceManager.getTexture(tex.name);

		int size = tex.size;
		cloud = new TextureRegion[size];

		for (int i = 0; i < size; i++) {
			cloud[i] = getTexFromData(texture, tex.reg[i]);
		}
	}

	private static void loadBackground() {
		GameTexture tex = SceneManager.gameTexture(Prop.TEX_BG);
		Texture texture = ResourceManager.getTexture(tex.name);

		// game elements textures
		bg = getTexFromData(texture, tex.reg[0]);
		grass = getTexFromData(texture, tex.reg[1]);
	}
	
	private static void loadPipe() {
		GameTexture tex = SceneManager.gameTexture(Prop.TEX_PIPE);
		Texture texture = ResourceManager.getTexture(tex.name);

		// game elements textures
		skullUp = getTexFromData(texture, tex.reg[0]);

		// Create by flipping existing skullUp
		skullDown = new TextureRegion(skullUp);
		skullDown.flip(false, true);

		bar = getTexFromData(texture, tex.reg[1]);
	}

	private static void loadSprites() {
		GameTexture tex = SceneManager.gameTexture(Prop.TEX_SPRITE);
		Texture texture = ResourceManager.getTexture(tex.name);
		
		// buttons textures
		playButtonUp = getTexFromData(texture, tex.reg[0]);
		playButtonDown = getTexFromData(texture, tex.reg[1]);
		
		// text
		ready = getTexFromData(texture, tex.reg[2]);
		retry = getTexFromData(texture, tex.reg[3]);
		gameOver = getTexFromData(texture, tex.reg[4]);
		highScore = getTexFromData(texture, tex.reg[5]);

		// scoreboard
		scoreboard = getTexFromData(texture, tex.reg[6]);
		starOn = getTexFromData(texture, tex.reg[7]);
		starOff = getTexFromData(texture, tex.reg[8]);
	}

	private static TextureRegion getTexFromData(Texture texture, SimpleData data) {
		TextureRegion texRegion = new TextureRegion(texture, data.x, data.y, data.w, data.h);
		texRegion.flip(false, true);

		return texRegion;
	}
}

