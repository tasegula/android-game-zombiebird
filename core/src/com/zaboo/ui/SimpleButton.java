package com.zaboo.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.zaboo.config.Prop;
import com.zaboo.config.ResourceManager;

public class SimpleButton {

	private final float x, y, width, height;

	private final TextureRegion buttonUp;
	private final TextureRegion buttonDown;

	private final Rectangle bounds;

	private boolean isPressed = false;

	public SimpleButton(final float x, final float y,
						final float width, final float height,
						final TextureRegion buttonUp,
						final TextureRegion buttonDown) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.buttonUp = buttonUp;
		this.buttonDown = buttonDown;

		bounds = new Rectangle(x, y, width, height);
	}

	public boolean isClicked(final int screenX, final int screenY) {
		return bounds.contains(screenX, screenY);
	}

	public void draw(final SpriteBatch batcher) {
		if (isPressed) {
			batcher.draw(buttonDown, x, y, width, height);
		} else {
			batcher.draw(buttonUp, x, y, width, height);
		}
	}

	public boolean isTouchDown(final int screenX, final int screenY) {

		if (bounds.contains(screenX, screenY)) {
			isPressed = true;
			return true;
		}

		return false;
	}

	public boolean isTouchUp(final int screenX, final int screenY) {

		// It only counts as a touchUp if the button is in a pressed state.
		if (bounds.contains(screenX, screenY) && isPressed) {
			isPressed = false;
			ResourceManager.getSound(Prop.AUDIO_FLAP).play();
			return true;
		}

		// Whenever a finger is released, we will cancel any presses.
		isPressed = false;
		return false;
	}
}
