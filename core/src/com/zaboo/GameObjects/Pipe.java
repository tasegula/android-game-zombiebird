package com.zaboo.GameObjects;

import java.util.Random;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

public class Pipe extends Scrollable {
	
	private static final Random rand = new Random();
	
	private final Rectangle skullUp, skullDown, barUp, barDown;
	
	public static final int VERTICAL_GAP = 45;
	public static final int SKULL_WIDTH = 24;
	public static final int SKULL_HEIGHT = 11;
	
	private final float groundY;
	
	private boolean isScored = false;
	
	public Pipe(final float x, final float y,
				final int width, final int height,
				final float scrollSpeed, final float groundY) {
		
		super(x, y, width, rand.nextInt(height) + 15, scrollSpeed);
		
		
		skullUp = new Rectangle();
		skullDown = new Rectangle();
		barUp = new Rectangle();
		barDown = new Rectangle();
		
		this.groundY = groundY;
	}
	
	@Override
	public void update(final float delta) {
		super.update(delta);
		
		// set the top left corner's x, y coordinates,
		// width and height of the rectangle
		barUp.set(position.x, position.y, width, height);
		barDown.set(position.x, position.y + height + VERTICAL_GAP,
					width, groundY - (position.y + height + VERTICAL_GAP));
		
		// skull width = 24; bar = 22 pixels wide =>
		// the skull must be shifted by 1 pixel to the left
		// (so that the skull is centered with respect to its bar).
		
		// This shift is equivalent to: (SKULL_WIDTH - width) / 2
		final float shiftSize = (SKULL_WIDTH - width) / 2;
		
		skullUp.set(position.x - shiftSize, position.y + height - SKULL_HEIGHT,
					SKULL_WIDTH, SKULL_HEIGHT);
		skullDown.set(position.x - shiftSize, barDown.y,
					  SKULL_WIDTH, SKULL_HEIGHT);
	}
	
	@Override
	public void reset(final float newX) {
		// Call superclass reset()
		super.reset(newX);
		
		// Change height to a random number
		height = rand.nextInt(90) + 15;
		isScored = false;
	}
	
	@Override
	public void onRestart(final float x, final float scrollSpeed) {
		velocity.x = scrollSpeed;
		reset(x);
	}
	
	public Rectangle getSkullUp() {
		return skullUp;
	}
	
	public Rectangle getSkullDown() {
		return skullDown;
	}
	
	public Rectangle getBarUp() {
		return barUp;
	}
	
	public Rectangle getBarDown() {
		return barDown;
	}
	
	public boolean collides(final Bird bird) {
		if (position.x < bird.getX() + bird.getWidth()) {
			return Intersector.overlaps(bird.getBoundingCircle(), barUp)
				   || Intersector.overlaps(bird.getBoundingCircle(), barDown)
				   || Intersector.overlaps(bird.getBoundingCircle(), skullUp)
				   || Intersector.overlaps(bird.getBoundingCircle(), skullDown);
		}
		return false;
	}
	
	public boolean isScored() {
		return isScored;
	}
	
	public void setScored(final boolean b) {
		isScored = b;
	}
}
