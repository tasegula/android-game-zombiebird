package com.zaboo.GameObjects;

import java.util.Random;

import com.badlogic.gdx.math.Rectangle;

public class Cloud extends Scrollable {
	
	private static final Random rand = new Random();
	float originalX;
	
	// private final Rectangle skullUp, skullDown, barUp, barDown;
	private final Rectangle cloud;
	
	public Cloud(final float x, final float y,
				 final int width, final int height,
				 final float scrollSpeed) {
		super(x, rand.nextInt(90), width, height, scrollSpeed);
		
		originalX = x;
		cloud = new Rectangle();
	}
	
	@Override
	public void update(final float delta) {
		super.update(delta);
		
		// set the top left corner's x, y coordinates, width and height of the
		// rectangle
		cloud.set(position.x, position.y, width, height);
	}
	
	@Override
	public void reset(final float newX) {
		// Call superclass reset()
		super.reset(originalX);
		
		// Change height to a random number
		position.y = rand.nextInt(90);
	}
	
	@Override
	public void onRestart(final float x, final float scrollSpeed) {
		velocity.x = scrollSpeed;
		reset(x);
	}
	
	public Rectangle getRectangle() {
		return cloud;
	}
}