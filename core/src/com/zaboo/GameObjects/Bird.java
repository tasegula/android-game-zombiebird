package com.zaboo.GameObjects;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.zaboo.config.Prop;
import com.zaboo.config.ResourceManager;

public class Bird {

	private final Vector2 position;
	private final Vector2 velocity;
	private final Vector2 acceleration;

	private float rotation; // For handling bird rotation
	private final int width;
	private final float height;

	private final float originalY;
	private boolean isAlive;

	private final Circle boundingCircle;

	public Bird(final float x, final float y, final int width, final int height) {
		this.width = width;
		this.height = height;

		this.originalY = y;

		this.position = new Vector2(x, y);
		this.velocity = new Vector2(0, 0);
		this.acceleration = new Vector2(0, 460);

		this.boundingCircle = new Circle();

		this.isAlive = true;
	}

	public void update(final float delta) {
		velocity.add(acceleration.cpy().scl(delta));

		if (velocity.y > 200) {
			velocity.y = 200;
		}

		// CEILING CHECK
		if (position.y < -13) {
			position.y = -13;
			velocity.y = 0;
		}

		position.add(velocity.cpy().scl(delta));

		// Update the circle of 6.5f radius
		boundingCircle.set(position.x + 9, position.y + 6, 6.5f);

		// Rotate counterclockwise
		if (velocity.y < 0) {
			rotation -= 600 * delta;
			if (rotation < -20) {
				rotation = -20;
			}
		}

		// Rotate clockwise
		if (isFalling() || !isAlive) {
			rotation += 480 * delta;
			if (rotation > 90) {
				rotation = 90;
			}
		}
	}

	public void updateReady(final float runTime) {
		position.y = 2 * (float) Math.sin(7 * runTime) + originalY;
	}

	public boolean isFalling() {
		return velocity.y > 110;
	}

	public boolean shouldFlap() {
		return velocity.y <= 70 && isAlive;
	}

	public void onClick() {
		if (isAlive) {
			ResourceManager.getSound(Prop.AUDIO_FLAP).play();
			velocity.y = -140;
		}
	}

	public void die() {
		isAlive = false;
		velocity.y = 0;
	}

	public void decelerate() {
		// We want the bird to stop accelerating downwards once it is dead.
		acceleration.y = 0;
	}

	public void onRestart(final int y) {
		rotation = 0;
		position.y = y;
		velocity.x = 0;
		velocity.y = 0;
		acceleration.x = 0;
		acceleration.y = 460;
		isAlive = true;
	}

	public float getX() {
		return position.x;
	}

	public float getY() {
		return position.y;
	}

	public int getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public float getRotation() {
		return rotation;
	}

	public Circle getBoundingCircle() {
		return boundingCircle;
	}

	public boolean isAlive() {
		return isAlive;
	}
}
