package com.zaboo.GameObjects;

import com.badlogic.gdx.math.Vector2;

public class Scrollable {
	
	protected Vector2 position;
	protected Vector2 velocity;
	protected int width;
	protected int height;
	protected boolean isScrolledLeft;
	
	protected float originalX;
	protected float speed;

	public Scrollable(final float x, final float y,
					  final int width, final int height,
					  final float scrollSpeed) {
		position = new Vector2(x, y);
		velocity = new Vector2(scrollSpeed, 0);
		this.width = width;
		this.height = height;
		isScrolledLeft = false;
	}
	
	public void update(final float delta) {
		position.add(velocity.cpy().scl(delta));
		
		// If the Scrollable object is no longer visible:
		if (position.x + width < 0) {
			isScrolledLeft = true;
		}
	}
	
	// Should Override for more specific behavior
	public void reset(final float newX) {
		position.x = newX;
		isScrolledLeft = false;
	}
	
	public void stop() {
		velocity.x = 0;
	}
	
	public void onRestart(final float x, final float scrollSpeed) {
		position.x = x;
		velocity.x = scrollSpeed;
	}

	// ************************************************************************
	// Getters
	// ************************************************************************
	
	public boolean isScrolledLeft() {
		return isScrolledLeft;
	}
	
	public float getTailX() {
		return position.x + width;
	}
	
	public float getX() {
		return position.x;
	}
	
	public float getY() {
		return position.y;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
}
