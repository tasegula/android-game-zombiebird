package com.zaboo.GameObjects;

import com.zaboo.GameWorld.GameWorld;
import com.zaboo.config.Prop;
import com.zaboo.config.ResourceManager;

public class ScrollHandler {
	
	private final GameWorld gameWorld;
	
	// Will create all needed objects
	private final Grass[] grass = new Grass[2];
	private final Pipe[] pipe = new Pipe[3];
	private final Cloud[] cloud = new Cloud[3];
	
	// CONSTANTS
	public static final int SCROLL_SPEED = -59;
	public static final int GAP = 49;
	
	// @param yPos tells where to create the Grass and Pipe objects
	public ScrollHandler(final GameWorld gameWorld, final float yPos) {
		this.gameWorld = gameWorld;
		
		grass[0] = new Grass(0, yPos, 143, 11, SCROLL_SPEED);
		grass[1] = new Grass(grass[0].getTailX(), yPos, 143, 11, SCROLL_SPEED);
		
		cloud[0] = new Cloud(210, 0, 30, 17, SCROLL_SPEED + 10);
		cloud[1] = new Cloud(cloud[0].getTailX() + GAP, 0, 24, 11, SCROLL_SPEED - 10);
		cloud[2] = new Cloud(cloud[1].getTailX() + GAP, 0, 12, 7, SCROLL_SPEED - 30);

		pipe[0] = new Pipe(210, 0, 22, 90, SCROLL_SPEED, yPos);
		pipe[1] = new Pipe(pipe[0].getTailX() + GAP, 0, 22, 90, SCROLL_SPEED, yPos);
		pipe[2] = new Pipe(pipe[1].getTailX() + GAP, 0, 22, 90, SCROLL_SPEED, yPos);
		
	}
	
	public void updateReady(final float delta) {
		updateCloud(delta);
		updateGrass(delta);
	}
	
	public void update(final float delta) {
		updateCloud(delta);
		updateGrass(delta);
		updatePipe(delta);
	}
	
	public void stop() {
		int i, size;
		
		size = grass.length;
		for (i = 0; i < size; i++) {
			grass[i].stop();
		}
		
		size = pipe.length;
		for (i = 0; i < size; i++) {
			pipe[i].stop();
		}
		
		size = cloud.length;
		for (i = 0; i < size; i++) {
			cloud[i].stop();
		}
	}
	
	// Return true if ANY pipe hits the bird.
	public boolean collides(final Bird bird) {
		
		int i, size = pipe.length;
		for (i = 0; i < size; i++) {
			if (testCollision(bird, pipe[i])) {
				addScore(1);
				pipe[i].setScored(true);
				ResourceManager.getSound(Prop.AUDIO_COIN).play();
				break;
			}
		}
		
		return pipe[0].collides(bird) || pipe[1].collides(bird) || pipe[2].collides(bird);
	}
	
	private boolean testCollision(final Bird bird, final Pipe pipe) {
		return !pipe.isScored()
			   && (pipe.getX() + pipe.getWidth() / 2) < (bird.getX() + bird.getWidth());
	}
	
	private void addScore(final int increment) {
		gameWorld.addScore(increment);
	}
	
	public Grass getGrass(int i) {
		return grass[i];
	}
	
	public Pipe getPipe(int i) {
		return pipe[i];
	}
	
	public Cloud getCloud(int i) {
		return cloud[i];
	}
	
	public void onRestart() {

		grass[0].onRestart(0, SCROLL_SPEED);
		grass[1].onRestart(grass[0].getTailX(), SCROLL_SPEED);

		pipe[0].onRestart(210, SCROLL_SPEED);
		pipe[1].onRestart(pipe[0].getTailX() + GAP, SCROLL_SPEED);
		pipe[2].onRestart(pipe[1].getTailX() + GAP, SCROLL_SPEED);
		
		cloud[0].onRestart(210, SCROLL_SPEED + 10);
		cloud[1].onRestart(cloud[0].getTailX() + GAP, SCROLL_SPEED - 10);
		cloud[2].onRestart(cloud[1].getTailX() + GAP, SCROLL_SPEED - 30);
	}
	
	public void updateCloud(float delta) {
		int i, size = cloud.length;

		for (i = 0; i < size; i++) {
			cloud[i].update(delta);
		}
		
		for (i = 0; i < size; i++) {
			if (cloud[i].isScrolledLeft()) {
				cloud[i].reset(210);
			}
		}
	}
	
	public void updateGrass(float delta) {
		int i, size = grass.length;
		
		for (i = 0; i < size; i++) {
			grass[i].update(delta);
		}

		for (i = 0; i < size; i++) {
			if (grass[i].isScrolledLeft()) {
				int x = (((i - 1) % size) + size) % size;
				grass[i].reset(grass[x].getTailX());
			}
		}
	}
	
	public void updatePipe(float delta) {
		// Check if any of the pipes are scrolled left, and reset accordingly
		int i, size = pipe.length;
		
		for (i = 0; i < size; i++) {
			pipe[i].update(delta);
		}

		for (i = 0; i < size; i++) {
			if (pipe[i].isScrolledLeft()) {
				int x = (((i - 1) % size) + size) % size;
				pipe[i].reset(pipe[x].getTailX() + GAP);
			}
		}
	}
}
