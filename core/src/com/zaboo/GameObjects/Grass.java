package com.zaboo.GameObjects;

public class Grass extends Scrollable {
	
	public Grass(final float x, final float y,
				 final int width, final int height,
				 final float scrollSpeed) {
		super(x, y, width, height, scrollSpeed);
	}
	
	@Override
	public void onRestart(final float x, final float scrollSpeed) {
		position.x = x;
		velocity.x = scrollSpeed;
	}
}
