package com.zaboo.config.deprecated;

@Deprecated
public class ReaderObject {
	
	public int[] xy = new int[2];
	public int[] dim = new int[2];
	
	public ReaderObject() {
		xy = new int[2];
		dim = new int[2];
	}
}
