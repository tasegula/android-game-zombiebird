package com.zaboo.config.deprecated;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.zaboo.config.GameData.GameTexture;
import com.zaboo.config.GameData.Pair;
import com.zaboo.config.GameData.SimpleData;

@Deprecated
public class ConfigXml {
	
	private static XmlReader    prop;
	private static final String filename = "data/config.xml";
	
	public static GameTexture bird, cloud, logo;
	public static GameTexture bg;
	public static GameTexture pipe;
	
	/**
	 * <p> sprites.so[0] = playButtonOn;
	 * <p> sprites.so[1] = playButtonOff;
	 * <p> sprites.so[2] = textReady;
	 * <p> sprites.so[3] = textRetry;
	 * <p> sprites.so[4] = textGameOver;
	 * <p> sprites.so[5] = textHighScore;
	 * <p> sprites.so[6] = board;
	 * <p> sprites.so[7] = boardStarOn;
	 * <p> sprites.so[8] = boardStarOff;
	 */
	public static GameTexture sprites;

	public static void load() {
		prop = new XmlReader();
		
		try {
			Element root = prop.parse(Gdx.files.internal(filename));
			
			getTextureData(root.getChildByName(PropXml.TEXTURES));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void getTextureData(Element element) {
		Array<Element> items = element.getChildrenByName(PropXml.ITEM);

		bird = getTextureConfig(PropXml.BIRD.value, items.get(PropXml.BIRD.key));
		cloud = getTextureConfig(PropXml.CLOUD.value, items.get(PropXml.CLOUD.key));
		bg = getTextureConfig(PropXml.BG.value, items.get(PropXml.BG.key));
		pipe = getTextureConfig(PropXml.PIPE.value, items.get(PropXml.PIPE.key));
		
		logo = getTextureConfig(PropXml.LOGO.value, items.get(PropXml.LOGO.key));
		
		sprites = getTextureConfig(PropXml.SPRITE.value, items.get(PropXml.SPRITE.key));
	}


	private static GameTexture getTextureConfig(String type, Element element) {
		String name = element.getAttribute(PropXml.NAME);
		if (!name.equals(type)) {
			return null;
		}
		
		int size = Integer.valueOf(element.getAttribute(PropXml.SIZE));
		GameTexture texture = new GameTexture(size);
		texture.name = name;

		Array<Element> minions = element.getChildrenByName(PropXml.SIMPLE);
		for (int i = 0; i < size; i++) {
			Element minion = minions.get(i);
			texture.reg[i] = getTextureRegion(minion);
		}
		
		return texture;
	}

	private static SimpleData getTextureRegion(Element element) {
		SimpleData result = new SimpleData();
		
		result.x = element.getIntAttribute("x");
		result.y = element.getIntAttribute("y");
		result.w = element.getIntAttribute("w");
		result.h = element.getIntAttribute("h");
		
		return result;
	}
	
	@SuppressWarnings("unused")
	private static final class PropXml {
		
		public static final String SIMPLE   = "simple";
		public static final String ITEM = "item";
		public static final String NAME = "name";

		public static final String LOCATION = "location";
		public static final String SIZE     = "size";
		
		public static final Pair<Integer, String> BIRD = new Pair<Integer, String>(0, "bird");
		public static final Pair<Integer, String> CLOUD = new Pair<Integer, String>(1, "cloud");
		public static final Pair<Integer, String> BG = new Pair<Integer, String>(2, "bg");
		public static final Pair<Integer, String> PIPE = new Pair<Integer, String>(3, "pipe");
		public static final Pair<Integer, String> LOGO = new Pair<Integer, String>(4, "logo");
		public static final Pair<Integer, String> SPRITE = new Pair<Integer, String>(5, "sprites");

		public static final String TEXTURES = "textures";
	}

	private ConfigXml() {
	}
}
