package com.zaboo.config.deprecated;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Deprecated
public class ConfigReader {
	
	private static Properties prop;
	
	public static String cloudTexLoc, bgTexLoc, logoTexLoc;
	public static String birdTexLoc, pipeTexLoc;

	public static ReaderObject[] bird, cloud;
	public static ReaderObject bg, grass;
	public static ReaderObject pipeBar, pipeBase;

	public static void load() {
		FileInputStream input;
		int i;

		String auxProperty;
		int auxValue;
		
		prop = new Properties();
		try {
			File f = new File("stef.tase");
			System.out.println(f.createNewFile() + ": " + f.getAbsolutePath());

			input = new FileInputStream("../android/assets/data/config.properties");
			
			// load a properties file
			prop.load(input);
			
			birdTexLoc = prop.getProperty(PropName.BIRD + PropName.LOCATION);
			cloudTexLoc = prop.getProperty(PropName.CLOUD + PropName.LOCATION);
			bgTexLoc = prop.getProperty(PropName.BG + PropName.LOCATION);

			getBirdConfig();
			getPipeConfig();

			auxProperty = prop.getProperty(PropName.CLOUD + PropName.SIZE);
			auxValue = Integer.valueOf(auxProperty);
			
			cloud = new ReaderObject[auxValue];
			for (i = 0; i < 3; i++) {
				cloud[i] = new ReaderObject();
				cloud[i].xy = getXY(PropName.CLOUD, i);
				cloud[i].dim = getDim(PropName.CLOUD, i);
			}
			
			bg = new ReaderObject();
			bg.xy = getXY(PropName.BG, -1);
			bg.dim = getDim(PropName.BG, -1);
			
			grass = new ReaderObject();
			grass.xy = getXY(PropName.GRASS, -1);
			grass.dim = getDim(PropName.GRASS, -1);

			input.close();
		}
		catch (final IOException ex) {
			System.out.println("qwertyuio!");
			ex.printStackTrace();
			return;
		}
		
	}
	
	private static void getBirdConfig() {
		int auxValue;
		int[] auxVector;
		
		auxValue = Integer.valueOf(prop.getProperty(PropName.BIRD + PropName.SIZE));
		auxVector = getDim(PropName.BIRD, -1);
		
		bird = new ReaderObject[auxValue];
		for (int i = 0; i < bird.length; i++) {
			bird[i] = new ReaderObject();
			bird[i].xy = getXY(PropName.BIRD, i);
			bird[i].dim = auxVector;
		}
	}
	
	private static void getPipeConfig() {
		String auxProp;
		
		pipeTexLoc = prop.getProperty(PropName.PIPE + PropName.LOCATION);
		
		pipeBar = new ReaderObject();
		pipeBase = new ReaderObject();
		
		auxProp = PropName.PIPE + ".bar";
		pipeBar.xy = getXY(auxProp, -1);
		pipeBar.dim = getDim(auxProp, -1);
		
		auxProp = PropName.PIPE + ".base";
		pipeBase.xy = getXY(auxProp, -1);
		pipeBase.dim = getDim(auxProp, -1);
		
	}

	private static int[] getXY(String type, int i) {
		int[] result = new int[2];
		String index = (i < 0) ? "" : "" + i;

		result[0] = Integer.valueOf(prop.getProperty(type + index + ".x"));
		result[1] = Integer.valueOf(prop.getProperty(type + index + ".y"));
		
		System.out.println(type + index + ".x: " + result[0]);
		System.out.println(type + index + ".y: " + result[1]);
		
		return result;
	}

	private static int[] getDim(String type, int i) {
		int[] result = new int[2];
		String index = (i < 0) ? "" : "" + i;
		

		result[0] = Integer.valueOf(prop.getProperty(type + index + PropName.WIDTH));
		result[1] = Integer.valueOf(prop.getProperty(type + index + PropName.HEIGHT));
		
		System.out.println(type + index + PropName.WIDTH + ": " + result[0]);
		System.out.println(type + index + PropName.HEIGHT + ": " + result[1]);

		return result;
	}

	private static final class PropName {
		
		public static final String PIPE = "pipe";
		public static final String BIRD = "bird";
		public static final String BG = "bg";
		public static final String CLOUD = "cloud";
		public static final String GRASS = "grass";
		
		public static final String LOCATION = ".location";
		public static final String SIZE = ".size";

		public static final String WIDTH = ".width";
		public static final String HEIGHT = ".height";
	}

	private ConfigReader() {
	}
}
