package com.zaboo.config;

import com.badlogic.gdx.Gdx;

public class Util {
	
	public static void print(Object message) {
		String header = "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
		Gdx.app.log("", header);
		Gdx.app.log("", "" + message);
		Gdx.app.log("", header);
	}
	
	private Util() {
	}
}
