package com.zaboo.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.zaboo.config.GameData.GameTexture;
import com.zaboo.config.GameData.SimpleData;

public class SceneManager {

	private static XmlReader prop;
	private static final String filename = "data/SceneManager.xml";

	private static Map<String, GameTexture> gameTextureMap;

	private SceneManager() {
	}

	public static void load() {
		prop = new XmlReader();

		try {
			Element root = prop.parse(Gdx.files.internal(filename));

			getGameObjectsData(root.getChildByName("objects"));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ************************************************************************
	// HELPERS
	// ************************************************************************

	private static void getGameObjectsData(Element element) {
		Array<Element> items = element.getChildrenByName(Prop.TAG_ITEM);

		gameTextureMap = new HashMap<String, GameTexture>(items.size);

		for (Element item: items) {
			GameTexture tex = getTextureConfig(item);
			gameTextureMap.put(tex.name, tex);
		}
	}

	private static GameTexture getTextureConfig(Element element) {
		String name = element.getAttribute(Prop.ATR_NAME);
		int size = Integer.valueOf(element.getAttribute(Prop.ATR_SIZE));

		GameTexture texture = new GameTexture(size);
		texture.name = name;
		
		Array<Element> minions = element.getChildrenByName(Prop.TAG_SIMPLE);
		for (int i = 0; i < size; i++) {
			Element minion = minions.get(i);
			texture.reg[i] = getTextureRegion(minion);
		}

		return texture;
	}

	private static SimpleData getTextureRegion(Element element) {
		SimpleData result = new SimpleData();

		result.x = element.getIntAttribute("x");
		result.y = element.getIntAttribute("y");
		result.w = element.getIntAttribute("w");
		result.h = element.getIntAttribute("h");

		return result;
	}

	// ************************************************************************
	// API - GETTERS
	// ************************************************************************

	public static Map<String, GameTexture> getGameTextureMap() {
		return gameTextureMap;
	}

	public static GameTexture gameTexture(String key) {
		return gameTextureMap.get(key);
	}
}
