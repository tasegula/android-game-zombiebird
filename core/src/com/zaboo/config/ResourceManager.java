package com.zaboo.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

public class ResourceManager {
	
	private static XmlReader prop;
	private static final String filename = "data/ResourceManager.xml";
	
	private static Map<String, Texture> textureMap;
	private static Map<String, Sound> soundMap;
	private static Map<String, BitmapFont> fontMap;
	
	public static void load() {
		prop = new XmlReader();
		
		try {
			Element root = prop.parse(Gdx.files.internal(filename));
			
			getTextureData(root.getChildByName(Folder.TEXTURES));
			getSoundData(root.getChildByName(Folder.AUDIO));
			getFontData(root.getChildByName(Folder.FONTS));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void getTextureData(Element element) {
		int size = element.getIntAttribute(Prop.ATR_SIZE);
		textureMap = new HashMap<String, Texture>(size);
		
		Array<Element> items = element.getChildrenByName(Prop.TAG_ITEM);
		
		for (Element item : items) {
			String name = item.getAttribute(Prop.ATR_NAME);
			String location = item.getAttribute(Prop.ATR_LOCATION);
			String filter = item.getAttribute(Prop.ATR_FILTER);
			
			String file = Folder.TEXTURES + "/" + location;
			Texture texture = new Texture(Gdx.files.internal(file));

			TextureFilter texFilter = getFilter(filter);
			texture.setFilter(texFilter, texFilter);
			
			textureMap.put(name, texture);
		}
	}
	
	private static TextureFilter getFilter(String filter) {
		if (filter.equals("linear")) {
			return TextureFilter.Linear;
		}
		else if (filter.equals("nearest")) {
			return TextureFilter.Nearest;
		}

		return null;
	}

	private static void getSoundData(Element element) {
		int size = element.getIntAttribute(Prop.ATR_SIZE);
		
		soundMap = new HashMap<String, Sound>(size);
		
		Array<Element> items = element.getChildrenByName(Prop.TAG_ITEM);
		
		for (Element item : items) {
			String name = item.getAttribute(Prop.ATR_NAME);
			String location = item.getAttribute(Prop.ATR_LOCATION);
			
			String file = Folder.AUDIO + "/" + location;
			Sound sound = Gdx.audio.newSound(Gdx.files.internal(file));
			
			soundMap.put(name, sound);
		}
	}
	
	private static void getFontData(Element element) {
		int size = element.getIntAttribute(Prop.ATR_SIZE);
		
		fontMap = new HashMap<String, BitmapFont>(size);
		
		Array<Element> items = element.getChildrenByName(Prop.TAG_ITEM);
		
		for (Element item : items) {
			String name = item.getAttribute(Prop.ATR_NAME);
			
			float x = item.getFloatAttribute("x");
			float y = item.getFloatAttribute("y");

			String file = Folder.FONTS + "/" + name + ".fnt";
			BitmapFont font = new BitmapFont(Gdx.files.internal(file));
			font.setScale(x, y);
			
			fontMap.put(name, font);
		}
	}

	// ************************************************************************
	// API
	// ************************************************************************
	
	public static Map<String, Texture> getTexturesMap() {
		return textureMap;
	}
	
	public static Texture getTexture(String name) {
		return textureMap.get(name);
	}

	public static Map<String, Sound> getSoundsMap() {
		return soundMap;
	}
	
	public static Sound getSound(String name) {
		return soundMap.get(name);
	}

	public static Map<String, BitmapFont> getFontsMap() {
		return fontMap;
	}
	
	public static BitmapFont getFont(String name) {
		return fontMap.get(name);
	}

	// ************************************************************************
	// HELPER CLASSES
	// ************************************************************************

	private static final class Folder {
		
		public static final String AUDIO = "audio";
		public static final String FONTS = "fonts";
		public static final String TEXTURES = "textures";
	}
	
	private ResourceManager() {
		
	}
}
