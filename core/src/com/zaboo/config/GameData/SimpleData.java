package com.zaboo.config.GameData;


public class SimpleData {
	
	public int x = 0;
	public int y = 0;
	public int w = 0;
	public int h = 0;
	
	@Override
	public String toString() {
		return "SimpleData [x=" + x + ", y=" + y + ", w=" + w + ", h=" + h + "]";
	}

}
