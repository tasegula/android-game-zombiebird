package com.zaboo.config.GameData;


public class Pair<K, V> {
	
	public K key;
	public V value;
	
	public Pair() {
		super();
	}
	
	public Pair(K id, V name) {
		this.key = id;
		this.value = name;
	}
	
	@Override
	public String toString() {
		return "Pair[" + key + ", " + value + "]";
	}

}
