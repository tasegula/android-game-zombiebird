package com.zaboo.config.GameData;

import java.util.Arrays;

public class GameTexture {
	public String name;

	public int size;
	public SimpleData[] reg;
	
	public GameTexture(int size) {
		this.size = size;
		this.reg = new SimpleData[size];
		
		for (int i = 0; i < size; i++) {
			this.reg[i] = new SimpleData();
		}
	}
	
	@Override
	public String toString() {
		return "GameTexture [name=" + name + ", size=" + size + ", reg=" + Arrays.toString(reg) + "]";
	}
	

}
