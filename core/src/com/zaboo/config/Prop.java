package com.zaboo.config;

public class Prop {
	
	public static final String TEX_BIRD = "bird";
	public static final String TEX_CLOUD = "cloud";
	public static final String TEX_BG = "bg";
	public static final String TEX_PIPE = "pipe";
	public static final String TEX_LOGO = "logo";
	public static final String TEX_SPRITE = "sprites";
	
	public static final String AUDIO_FLAP = "flap";
	public static final String AUDIO_COIN = "coin";
	public static final String AUDIO_FALL = "fall";
	public static final String AUDIO_DEAD = "dead";
	
	public static final String FONT_SHADOW = "shadow";
	public static final String FONT_WHITE = "whitetext";
	public static final String FONT_GREEN = "greentext";
	
	public static final String TAG_SIMPLE = "simple";
	public static final String TAG_ITEM = "item";
	
	public static final String ATR_LOCATION = "location";
	public static final String ATR_NAME = "name";
	public static final String ATR_SIZE = "size";
	public static final String ATR_ID = "id";
	public static final String ATR_FILTER = "filter";
}
