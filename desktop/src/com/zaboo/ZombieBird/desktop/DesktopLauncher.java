package com.zaboo.ZombieBird.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.zaboo.ZombieBird.ZBGame;

public class DesktopLauncher {
	
	public static void main(final String[] arg) {
		final LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.title = "Flying Bird";
		config.useGL30 = true;
		config.width = 272;
		config.height = 408;
		
		new LwjglApplication(new ZBGame(), config);
	}
}
